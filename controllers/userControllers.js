const User = require("../models/User");
//dependecy for encryoted password
const bcrypt = require("bcrypt");

const auth = require("../auth")
//Check if the email already exists


module.exports.checkemailExists = (reqBody) => {
	return User.find({ email: reqBody.email }).then(result => {

		if(result.length > 0) {
			return true
		} else {
			return false 
		}
	})
}


//user registration
/*Business Logic
1. Create a new  User object
2. Make sure that the password is encrypted
3. Save the new User to the database

*/ 

module.exports.registerUser = (reqBody) => {

	let newUser = new User({
		firstName : reqBody.firstName,
		lastName : reqBody.lastName,
		mobileNo : reqBody.mobileNo,
		email : reqBody.email,
		password : bcrypt.hashSync(reqBody.password, 10)
	})

	return newUser.save().then((user, error) => {
		if(error) {
			return false;
		} else 
		return true
	})
}


//User Authentication

/*
Steps:
1.  Check the database if the user email exists
2. Compare the password provided in the login form with the password stored in the database.
3. Generate/return a JSON web token if the user is successfully logged in and return false if not
*/

module.exports.loginUser = (reqBody) => {
	//findOne it will return the first record in the collection that matches the search criteria

	return User.findOne({ email: reqBody.email }).then(result => {
		if(result == null){
			return false;
		} else {


			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			if(isPasswordCorrect) {

				return { accessToken : auth.createAccessToken(result.toObject()) }
			} else {
				return false;
			}
		}
	})
}


module.exports.getProfile = (data) => {
	return User.findById(data).then(result => {
		
			result.password = "";
		
			return result;
		
	})
}